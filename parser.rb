require 'ostruct'
require 'csv'

class CSVResultOnSteroids
  class << self
    READERS = [:source, :csv, :record_accessors]

    def method_missing(method, *args, &block)
      if READERS.include?(method)
        read(method)
      else
        collection.send(method, *args, &block)
      end
    end

    private

    def collection
      get!
      Collection.new(@csv, @record_accessors)
    end

    def read(accessor)
      get!
      instance_variable_get("@#{accessor}".to_sym)
    end

    def set_source_with(path)
      @source = path
    end

    def get!
      @csv              = CSV.read(@source)
      @record_accessors = Accessor.extract_from(@csv)
    end
  end

  class Accessor
    def self.extract_from(csv)
      first_row = csv.first
      first_row.map &:downcase
    end
  end

  class Record < OpenStruct
    def initialize(options)
      super

      @accessors  = options[:accessors]
      @csv_data   = options[:csv_data]

      create_accessors!
    end

    private

    def create_accessors!
      @accessors.each_with_index do |accessor, index|
        new_ostruct_member accessor
        send "#{accessor}=", data_parser(@csv_data[index])
      end
    end

    def data_parser(data)
      _data = data ? data.to_s.split(',') : []
      _data.map(&:strip)
    end
  end

  class Collection
    include Enumerable

    def initialize(csv, accessors)
      @csv       = csv
      @accessors = accessors
      @data      = []

      load!
    end

    def each
      @data.each { |record| yield record }
    end

    def all
      to_a
    end

    def pluck(method)
      raise ArgumentError unless @accessors.include? method.to_s
      map{ |record| record.send(method) }.tap{ |_result| _result.flatten! }
    end

    def method_missing(method, *args)
      match = method.to_s.match(/find_by_(.+)/)
      match ? find_by(match[1], *args) : super
    end

  private

    def find_by(accessor, value)
      find{ |record| record.send(accessor).any?{ |data| data.to_s =~ /#{value}/i } }
    end

    def load!
      for index in 1..(@csv.size - 1)
        @data << Record.new(accessors: @accessors, csv_data: @csv[index])
      end
    end
  end
end
